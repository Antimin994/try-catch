const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  class BookCard {
    constructor(author, name, price) {
       this.author = author;
       this.name = name;
       this.price = price;
       this.card = null;
    }
 
    render() {
       this.card = document.createElement("ul");
       this.card.classList.add("card");
       this.card.innerHTML = `<li>
       <p>${this.author} "${this.name}" - ${this.price}</p>
       </li>`; 
       document.querySelector("#root").appendChild(this.card);   
    }
 
 }

  books.forEach(({author, name, price}) => {
    try {
        if (!author || !name || !price ) throw new Error();
        const bookCard = new BookCard(author, name, price);
        bookCard.render();
    }    
    catch(err) {
        console.log(err);
        return;
    }   
 });



